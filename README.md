# PT Launcher #

This git is for the Project Torque Launcher. Concept stage at the moment. 
It uses WPF from Visual Studios 2019

### Planned features  ###

* Links to website, forum, discord, steam community and to buy GP (90% done)
* News from Steam hub via RSS feed
* Language select via drop down or flag buttons (80% done)


### Extras being worked on ###

* Save langauge choice 
* Check login server online/ user can reach server, check uPnP is enabled and works and display this info with graphics

### More info ###

The initial commit contains code that not that great. 
It works but hopefully someone can make it much better and commit said changes to the git.

This code is shared under GNU General Public License v3.0
